#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys
import argparse
import pprint
import pandas as pd
from BCBio.GFF import GFFExaminer
from BCBio import GFF


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('gff',
                        type = str,
                        help = 'GFF3 format.')
    return parser.parse_args()


def main(argv):
    """
    Quick scan and print of gff data. Specifically, the parent-children structure map, number of records and lengths,
    and number of genes in records.
    :param argv:
    :return:
    """
    options = get_args()
    examiner = GFFExaminer()

    names = []
    lengths = []
    genes_n = []

    handle = open(options.gff, 'r')
    pprint.pprint(examiner.parent_child_map(handle))

    handle = open(options.gff, 'r')
    for record in GFF.parse(handle):
        names.append(record.id)
        lengths.append(len(record))
        genes_n.append(0)
        for feature in record.features:
            if feature.type == 'gene':
                genes_n[-1] += 1
    handle.close()

    df = pd.DataFrame(data={'name': names, 'length': lengths, 'genes': genes_n})

    print('\nNumber of records: ', len(df.name),

          '\n\nLargest record: ', df.name[df.length.idxmax],' ', df.length[df.length.idxmax],
          '\nShortest record: ', df.name[df.length.idxmin],' ', df.length[df.length.idxmin],
          '\nMean length: ', df.length.mean(),

          '\n\nMost genes in one record: ', df.name[df.genes.idxmax], ' ', df.genes[df.genes.idxmax],
          '\nLeast genes in one record: ', df.name[df.genes.idxmin], ' ', df.genes[df.genes.idxmin],
          '\nMean gene number: ', df.genes.mean(),

          '\n\nRecords with 3+ genes: ', len(df.genes[df.genes >= 3])
          )


if __name__ == "__main__":
    main(sys.argv)