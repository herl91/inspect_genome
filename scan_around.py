#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys
import argparse
import pandas as pd
from Bio import SeqIO


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('genome',
                        type = str,
                        help = 'Genbank format.')
    parser.add_argument('target',
                        type = str,
                        help = 'Must be a locus_tag')
    parser.add_argument('output',
                        type = str)
    parser.add_argument('--flank_size','-n',
                        type = int,
                        required = False,
                        default = 5,
                        help = '5 by default.')
    parser.add_argument('--flank_type','-f',
                        type = str,
                        required = False,
                        choices = ['genes','length'],
                        default = 'genes',
                        help = 'Length mode does not work, only use genes for now.')  # TODO
    return parser.parse_args()


def main(argv):
    options = get_args()

    d = {}

    # load data
    handle = open(options.genome, 'r')
    for record in SeqIO.parse(handle,'genbank'):
        for feature in record.features:
            if feature.type == 'gene':
                locus = feature.qualifiers['locus_tag'][0]
                d[locus] = {}

                if 'gene' in feature.qualifiers:
                    d[locus]['gene'] = feature.qualifiers['gene'][0]
                else:
                    d[locus]['gene'] = ''

                d[locus]['record'] = record.id
                d[locus]['start'] = feature.location.nofuzzy_start
                d[locus]['end'] = feature.location.nofuzzy_end
    handle.close()

    df = pd.DataFrame.from_dict(d, orient='index')
    df = df.sort_values(by=['record', 'start']).reset_index(drop=False).rename(columns = {'index':'locus_tag'})

    del d

    # inspect data
    if options.flank_type == 'genes':
        target_index = df[df.locus_tag == options.target].index

        target_record = df.ix[target_index].record
        left_flank = (target_index - options.flank_size)[0]
        right_flank = (target_index + options.flank_size)[0]

        record_mask = df.record.isin(target_record)
        subset = df[record_mask]

        import IPython
        IPython.embed()

        index_mask = (left_flank <= subset.index) & (subset.index <= right_flank)
        subset = subset[index_mask]

        subset.to_csv(options.output, sep=',', index=False)
    else:
        pass  # TODO


if __name__ == "__main__":
    main(sys.argv)